#!/usr/bin/env python3

import os
import sys
import time
import datetime
import csv
import os.path
import atexit
import signal
from yoctopuce.yocto_api import *
from yoctopuce.yocto_i2cport import *
from yoctopuce.yocto_poweroutput import *

# DEBUG variable to show or hide last error message received for undetected nodes
DEBUG = False # Set to True to show last error message received for each scanned channel for undected nodes

# Catch Ctrl-c events to quit gracefully
def handler(signum, frame):
    sys.exit("\nOperation cancelled.")
 
signal.signal(signal.SIGINT, handler)

# ANSI escape sequences for colors
class Colors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

### Functions
# Fancier prints
def print_colored(message, color):
    print(color + message + Colors.ENDC)

# Calculate time from minute or second inputs
def parse_time_input(input_str):
    """
    Parse a string input for time in minutes or seconds.
    Expects format like '10m' for 10 minutes or '30s' for 30 seconds.
    Defaults to minutes if no unit is provided.
    Returns time in seconds.
    """
    input_str = input_str.strip().lower()  # Normalize input
    if input_str.endswith('m'):
        # Extract number part and convert to minutes
        return int(input_str[:-1]) * 60
    elif input_str.endswith('s'):
        # Extract number part and convert directly to seconds
        return int(input_str[:-1])
    else:
        # No unit provided, default to minutes
        return int(input_str) * 60 if input_str else 0

# Countdown function to show between phases
def countdown(collection_interval_s): 
    while collection_interval_s: 
        mins, secs = divmod(collection_interval_s, 60) 
        timer = '{:02d}:{:02d}'.format(mins, secs)
        print(f"Next phase in", timer, end="\r") 
        time.sleep(1) 
        collection_interval_s -= 1

# Data conversion functions
def decode_voltage(voltage_byte):
    """
    Convertit une valeur brute de tension en tension réelle.
    
    :param voltage_byte: Valeur brute de tension.
    :return: Tension réelle arrondie à 3 chiffres après la virgule.
    """
    vin = (voltage_byte * 5612 / 254) / 1000  # Conversion basée sur le mappage Arduino
    return round(vin, 3)

def decode_temperature(temp_bytes):
    """
    Convertit une valeur brute de température en degrés Celsius.
    
    :param temp_bytes: Array de 2 octets représentant la température.
    :return: Température en degrés Celsius arrondie à 3 chiffres après la virgule ou NA si non valide.
    """
    if all(byte == 255 for byte in temp_bytes):  # Gère les valeurs non valides
        return "NA"
    temp_cK = temp_bytes[1] << 8 | temp_bytes[0]  # Calcul de la température en Kelvin
    temp_K = temp_cK / 100.0
    temp_C = temp_K - 273.15  # Conversion en Celsius
    return round(temp_C, 3)

def decode_humidity(hum_byte):
    """
    Convertit une valeur brute d'humidité en pourcentage.
    
    :param hum_byte: Valeur brute d'humidité.
    :return: Humidité en pourcentage arrondie à 3 chiffres après la virgule ou NA si non valide.
    """
    if hum_byte == 255:  # Gère les valeurs non valides
        return "NA"
    hum = hum_byte / 254.0 * 100
    return round(hum, 3)

def decode_lux(lux_bytes):
    """
    Convertit une valeur brute de luminosité en lux.
    
    :param lux_bytes: Array de 2 octets représentant la luminosité.
    :return: Luminosité en lux arrondie à 3 chiffres après la virgule ou NA si non valide.
    """
    if all(byte == 255 for byte in lux_bytes):  # Gère les valeurs non valides
        return "NA"
    lux_value = lux_bytes[1] << 8 | lux_bytes[0]
    lux = lux_value * 120000 / 65534
    return round(lux, 3)

def decode_ntc(raw_data):
    ntc_temp_indices = [
        (9, 10), (11, 12), (13, 14), (15, 16), (17, 18), (19, 20),
        (21, 22), (23, 24), (25, 26), (27, 28)
    ]
    decoded_values = []  # Initialize an empty list to collect decoded values
    for index, (low, high) in enumerate(ntc_temp_indices):
        # Append the decoded temperature to the list
        decoded_values.append(decode_temperature(raw_data[low:high+1]))
    return decoded_values  # Return the list of decoded values
    
# Setup I2C port
def setup_i2c_port():
    print_colored("  Setting up Yocto-i2c power and I2C port…", Colors.HEADER)
    i2cPort.set_i2cMode("1kbps,50ms")
    # Power sequence
    power.set_voltage(YPowerOutput.VOLTAGE_OFF) # Ensure that this is what is the default in the Yocto-i2c flash or it might end up in a stuck state due to the motherboard pulling too much at boot, possibly run a saveToFlash
    # power.get_module().saveToFlash()
    power.set_voltage(YPowerOutput.VOLTAGE_OUT1V8)
    time.sleep(0.1)
    i2cPort.set_i2cVoltageLevel(YI2cPort.I2CVOLTAGELEVEL_1V8)
    time.sleep(0.1)
    power.set_voltage(YPowerOutput.VOLTAGE_OUT3V3)
    time.sleep(0.1)
    i2cPort.set_i2cVoltageLevel(YI2cPort.I2CVOLTAGELEVEL_3V3)
    time.sleep(0.1)
    power.set_voltage(YPowerOutput.VOLTAGE_OUT5V)
    time.sleep(10)

# Channel (bus) opening
def open_channel(channel):
    print_colored(f"  Channel {channel}:", Colors.OKCYAN)
    chan = bytearray([1 << channel])
    try:
        i2cPort.i2cSendArray(0x70, chan)
        time.sleep(0.005)
        return True
    except Exception as e:
        print_colored(f"    Error opening channel {channel}: {e}", Colors.FAIL)
        return False

# Node detection
def scan_nodes():
    time.sleep(0.1)
    start_address = 0x20  # Define start address of the range
    end_address = 0x29    # Define end address of the range (exclusive in Python, inclusive in print)
    toSend = [0x00]
    found_nodes = []
    not_found_nodes = []
    last_error = ""  # Keep track of the last error message
    print_colored(f"    Scanning I2C nodes from {hex(start_address)} to {hex(end_address)}…", Colors.OKBLUE)
    
    for x in range(start_address, end_address + 1): # The end address is excluded in Python so we add 1
        try:
            received = i2cPort.i2cSendAndReceiveArray(x, toSend, 2)
            if received[0] == 0x00:
                found_nodes.append(x)  # Append integer value
            else:
                not_found_nodes.append(hex(x))  # Keep as hex for display
        except Exception as e:
            not_found_nodes.append(hex(x))  # Keep as hex for display
            last_error = str(e)  # Update last error message
 # Formatting the output
    if found_nodes:
        print_colored("      Found:      " + "  ".join(hex(node) for node in found_nodes), Colors.OKGREEN)
    if not_found_nodes:
        print_colored("      Not found:  " + "  ".join(not_found_nodes), Colors.ENDC)
        if DEBUG == True and last_error:
            print_colored("                  (" + last_error + ")", Colors.ENDC)
    return found_nodes
        
# Data collection
def collect_data_for_channel(channel=0):
    node_data = []
    if not open_channel(channel):
        print_colored(f"  Skipping channel {channel} not opening properly", Colors.WARNING)
        return []
    nodes = scan_nodes()
    if not nodes:
        print_colored(f"    Skipping channel {channel} (no nodes found).", Colors.OKBLUE)
        return []
    for node_address in nodes:
        attempt = 0
        while attempt <= 3:
            try:
                # Message for initial communication when doing the first attempt
                if attempt == 0:
                    print_colored(f"    Communicating with node {hex(node_address)}…", Colors.OKBLUE)         
                i2cmode = i2cPort.get_i2cMode().split(',')
                i2cPort.i2cSendArray(node_address, [0x04])
                # Default sleep is 1.5 seconds unless we're in a retry scenario
                time.sleep(1.5 if attempt == 0 else 5)
                raw_data = i2cPort.i2cSendAndReceiveArray(node_address, [0x05], 40)
                attempt += 1 # Increment attempt
                # print(f"Attempt", attempt, ":") #DEBUG
                # print([(i, n) for i,n in enumerate(raw_data)])  # DEBUG
                # print("Comment lines 198-199 in the script to remove this debug output") # DEBUG
                # Abandon if sensors still not ready after 3 attempts
                if raw_data[0] != 1 and attempt > 3:
                    print_colored("      Node sensors still not ready, skipping node.", Colors.FAIL)
                    pass # Continue the current round of the loop from there, since attempt > 3, the loop shouldn't restart for this nodee
                # Node not ready, prepare for retry
                if raw_data[0] != 1 and attempt <= 3:
                    print_colored(f"      Node sensors not ready, retry {attempt}/3 in 5 seconds…", Colors.WARNING)
                    pass # Continue the current round of the loop from there, since attempt < 3, the loop should restart for this node
                # If node was ready and returned data, or attempts are exhausted, process the data and append (although there should be no data if attempts all failed)
                if raw_data[0] == 1:
                    processed_data = [
                        hex(raw_data[1]),
                        decode_voltage(raw_data[3]),
                        decode_temperature(raw_data[4:6]),
                        decode_humidity(raw_data[6]),
                        decode_lux(raw_data[7:9]),
                        decode_ntc(raw_data)[0],
                        decode_ntc(raw_data)[1],
                        decode_ntc(raw_data)[2],
                        decode_ntc(raw_data)[3],
                        decode_ntc(raw_data)[4],
                        decode_ntc(raw_data)[5],
                        decode_ntc(raw_data)[6],
                        decode_ntc(raw_data)[7],
                        decode_ntc(raw_data)[8],
                        decode_ntc(raw_data)[9],
                        decode_temperature(raw_data[29:31]),
                        decode_humidity(raw_data[31]),
                        decode_lux(raw_data[32:34]),
                        decode_temperature(raw_data[34:36]), 
                        decode_humidity(raw_data[36]),
                        decode_lux(raw_data[37:39])
                        # Add other sensor data processing as needed
                    ]
                    node_data.append((channel, node_address, i2cmode, processed_data))
                    print_colored(f"      Data retrieved from node {hex(node_address)}.", Colors.OKGREEN)
                    break  # Exit while loop after processing data
            except Exception as e:
                print_colored(f"      Error: {e}", Colors.FAIL)
                break  # Exit while loop on error
    return node_data

### Run script
# Initial message
print_colored("Furgo nodes connected to the motherboard will be scanned, and data will be connected according to your input below.\nPress Ctrl-c at any moment to abort.", Colors.OKCYAN)
    
# Prompt for I2C station name
station_name = input("Enter I2C station name: ").replace(" ", "_")
csv_filename = f"{station_name}_i2c_intercalibration_data.csv"

# Handle CSV file existence: determine whether to append to or overwrite the file
file_mode = 'w'  # Default to write mode until file detection is performed, then append mode will be default if a pre-existing file is found
if os.path.exists(csv_filename):
    confirmation = input(f"\033[93m'{csv_filename}' already exists: [a]ppend (default), [o]verwrite, or [c]ancel? \033[39m").strip() or "a"
    if confirmation.lower() == 'a':
        file_mode = 'a'
        pass
    if confirmation.lower() == 'o':
        file_mode = 'w'  # Change to write mode
    if confirmation.lower() == 'c':
        sys.exit("Operation cancelled.")

# Prompt for number of collection phases, and interval between phases
number_of_collections = int(input("Enter the number of data collection phase(s) (default: 1): ").strip() or "1")

# Initialize variables for unit and value
unit = "minute"  # Default unit
value = "0"  # Default value

if number_of_collections > 1:
    collection_interval_input = input("Enter the interval between data collection phases (e.g., 10m for minutes, 30s for seconds, default: 0m): ").strip()
    # Only proceed if the user provided a non-empty input
    if collection_interval_input:
        # Determine the unit and value based on the input
        if 'm' in collection_interval_input:
            unit = "minute"
            value = collection_interval_input.replace("m", "")
        elif 's' in collection_interval_input:
            unit = "second"
            value = collection_interval_input.replace("s", "")
        else:
            # Assume minutes if no unit is specified
            value = collection_interval_input
    
    # Convert value to seconds based on the unit
    if unit == "minute":
        collection_interval_s = int(value) * 60
    elif unit == "second":
        collection_interval_s = int(value)
else:
    collection_interval_s = 0
    
# Initialize Yocto API
print_colored("Initializing Yocto API…", Colors.HEADER)
errmsg = YRefParam()
if YAPI.RegisterHub("usb", errmsg) != YAPI.SUCCESS: # Replace with "usb" if not using VirtualHub, else "127.0.0.1"
    sys.exit("  init error: " + errmsg.value)

# Check for connected I2C port
print_colored("Checking for connected I2C port…", Colors.HEADER)
i2cPort = YI2cPort.FirstI2cPort()
if i2cPort is None:
    sys.exit("  No Yocto-i2c detected (check USB cable and make sure VirtualHub is not taking priority)")

# Initialize power output
print_colored("Initializing Yocto-i2c power output…", Colors.HEADER)
power = YPowerOutput.FirstPowerOutput()
if power is None:
    sys.exit("  No power output module available")

# Begin data collection
# Adjust complement message based on number of collections and unit
if number_of_collections == 1:
    complement = ""
else:
    complement = f"s separated by {value}-{unit} intervals"

# Prompt the user to begin data collection
input(f"\033[1mPress Enter to begin data collection process with {number_of_collections} phase{complement}.\033[0m")
print_colored("--", Colors.OKCYAN)

# Loop for every data collection phase
for _ in range(number_of_collections):

    # Right before starting this round, capture the current datetime
    current_datetime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
    print_colored(f"\033[KPHASE {(_ + 1)}/{number_of_collections}, {current_datetime}", Colors.OKCYAN)

    # Setup I2C port and power
    start = time.time()
    setup_i2c_port()
    
    # Initialize an empty list to store data from all channels
    all_channel_data = []

    for channel in range(4):
        channel_data = collect_data_for_channel(channel)
        all_channel_data.extend(channel_data)

    print_colored("  Switching power output off…", Colors.HEADER)
    power.set_voltage(YPowerOutput.VOLTAGE_OFF)
            
    # Adjust the CSV writing part to include the datetime
    print_colored("  Writing collected data to CSV…", Colors.OKCYAN)
    with open(csv_filename, file_mode, newline='') as csvfile:
        fieldnames = ['channel', 'baud_rate', 'recovery_delay', 'address_request', 'address_reply',
                      'datetime', 'nodeid', 'voltage', 'temperature1', 'humidity1', 'light1',
                      'ntc01', 'ntc02', 'ntc03', 'ntc04', 'ntc05', 'ntc06', 'ntc07', 'ntc08', 'ntc09', 'ntc10',
                      'temperature2', 'humidity2', 'light2', 'temperature3', 'humidity3', 'light3']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        if file_mode == 'w':
            writer.writeheader() # Write headers only if in write mode
            file_mode = 'a' # Subsequent writes append data
        for channel, node_address, i2cmode, data in all_channel_data:
            # print(data) # DEBUG
            writer.writerow({
                'channel': channel, 
                'baud_rate': str(i2cmode[0]),
                'recovery_delay': str(i2cmode[1]),
                'address_request': hex(node_address),
                'address_reply': data[0], 
                'datetime': current_datetime,
                'nodeid': str(channel + 1) + str(node_address),
                'voltage': str(data[1]),
                'temperature1': str(data[2]),
                'humidity1': str(data[3]),
                'light1': str(data[4]),
                'ntc01': str(data[5]),
                'ntc02': str(data[6]),
                'ntc03': str(data[7]),
                'ntc04': str(data[8]),
                'ntc05': str(data[9]),
                'ntc06': str(data[10]),
                'ntc07': str(data[11]),
                'ntc08': str(data[12]),
                'ntc09': str(data[13]),
                'ntc10': str(data[14]),
                'temperature2': str(data[15]),
                'humidity2': str(data[16]),
                'light2': str(data[17]),
                'temperature3': str(data[18]),
                'humidity3': str(data[19]),
                'light3': str(data[20])
            })

    print_colored(f"    Data collection {(_ + 1)}/{number_of_collections} complete and saved to {csv_filename}.", Colors.OKGREEN)
    
    if _ < number_of_collections - 1:  # Wait before next phase if more are remaining
        end = time.time()
        print_colored("--", Colors.OKCYAN)
        time_remaining = int(collection_interval_s - (end - start))
        if time_remaining < 0:
            time_remaining = 0
        countdown(time_remaining)
       
YAPI.FreeAPI()    
